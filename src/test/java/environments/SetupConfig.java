package environments;


import java.io.FileReader;
import java.util.Properties;

public class SetupConfig
{

    public static Properties propObj;

    static FileReader inputStreamObj = null;
    public static  String url = null;
    //public static  String url2 = null;
    public static  String navigate = null;
    public static  String username = null;
    public static  String password = null;

    //STATIC METHOD
    public static void getDetails()throws Throwable{
        try
        {
            propObj = new Properties();
            inputStreamObj = new FileReader(System.getProperty("user.dir")+"/src/test/resources/application.properties");

            //LOAD AND READ THE PROPERTIES FILE FROM ENVIRONMENTS
            propObj.load(inputStreamObj);
            url = propObj.getProperty("url");
            //url2 = propObj.getProperty("url2");
            //navigate = propObj.getProperty("navigate");
           // username = propObj.getProperty("username");
            //password = propObj.getProperty("password");
        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if (inputStreamObj !=null){
                try {
                    inputStreamObj.close();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

}
