package steps;

import environments.SetupConfig;
import net.thucydides.core.annotations.Step;
import pages.loginPage;

public class endUser {

    loginPage pageObj;

//UNISA
    @Step
    public void openUrl()throws Throwable{
        pageObj.goToUrl();
        SetupConfig.getDetails();
       // pageObj.Login(SetupConfig.username, SetupConfig.password);
    }

    @Step
    public void Login()throws Throwable{
        pageObj.getLoginForUnisa();
    }
    @Step
    public void LoginStudentPortal()throws Throwable{
        pageObj.Login("48094560","mondee");
    }
    @Step
    public void links()throws Throwable{
        pageObj.getLinkNav();
    }
    //FLIGHT APPLICATION++++++++++++++++++++++++++++++++++++
    @Step
    public void selectFlight()throws Throwable{
        pageObj.flight();
    }
    @Step
    public void selectDate()throws Throwable{
        pageObj.datePicker();
    }
    @Step
    public void departure()throws Throwable {
        pageObj.departureDate();
    }
    //=======================================================
    @Step
    public void pickDate()throws Throwable {

    }


}
