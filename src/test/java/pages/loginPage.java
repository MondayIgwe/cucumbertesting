package pages;


import environments.SetupConfig;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import java.util.Calendar;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import  java.util.TimeZone;

public class loginPage extends PageObject {

    @Managed
    private WebDriver myDriver;
    private String today;

    private  int implicitWaitTimeout = 10000;

    //ELEMENTS
    //<editor fold desc ="login">
    @FindBy(xpath = "//button[contains(text(),'Click here to login')]")
    @CacheLookup
    private WebElementFacade hereToLogin;

    @FindBy(xpath = "//input[@id='eid']")
    @CacheLookup
    private WebElementFacade inputUsername;

    @FindBy(xpath = "//input[@id='pw']")
    @CacheLookup
    private WebElementFacade inputPassword;

    @FindBy(xpath = "//input[@value='Login']")
    @CacheLookup
    private WebElementFacade loginBtn;

    @FindBy(xpath = "//ul[@id='topnav']")
    @CacheLookup
    private WebElementFacade linkNav;


    @FindBy(xpath = "//div[@id='flightCriterias']")
    private  WebElementFacade searchCriteriaWrapper;

    //openPage
    @Step
    public void goToUrl()throws Throwable{
        myDriver = getDriver();
        setDriver(myDriver);
        SetupConfig.getDetails();
        myDriver.get(SetupConfig.url);
        myDriver.manage().window().maximize();
        myDriver.manage().timeouts().implicitlyWait(implicitWaitTimeout, TimeUnit.SECONDS);
        Thread.sleep(3000);
        myDriver.quit();
    }
    //HIGHLIGHT ELEMENTS
    @Step
    public void highLighterMethod(WebElement element){
        ((JavascriptExecutor)myDriver).executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
    }
    //Javscript Click an element
    @Step
    public void JavaScriptClickElement(WebElement element)
    {
        ((JavascriptExecutor)myDriver).executeScript("arguments[0].click();", element);
    }
    //SCROLL INTO ELEMENTS
    @Step
    public void scrollIntoElement(WebElement element) {
        ((JavascriptExecutor)myDriver).executeScript("arguments[0].scrollIntoView(true)", element);
    }
    //HIGHLIGHT ELEMENTS
    @Step
    public void highLighterBorderMethod(WebElement element) throws Throwable{
        ((JavascriptExecutor)myDriver).executeScript("js.executeScript(arguments[0].setAttribute('style','border: solid 3px white');", element);
    }
    //CLICK HERE TO LOGIN BUTTON
    @Step
    public void getLoginForUnisa() throws Throwable{
        if (hereToLogin.isCurrentlyEnabled()) {
            Actions actions = new Actions(myDriver);
            actions.moveToElement(hereToLogin).click().perform();
            System.out.print("page opened");
            Thread.sleep(3000);

        }
    }
    @Step
    public void Login(String username, String password) throws Throwable{
        try
        {
            highLighterMethod(inputUsername);
            inputUsername.sendKeys(username);
            // inputUsername.sendKeys(config);

            highLighterMethod(inputPassword);
            inputPassword.sendKeys(password);

            highLighterMethod(loginBtn);
            loginBtn.click();

            Thread.sleep(2000);

        }catch (NoSuchElementException e){
            System.out.print(e.getMessage());
        }

    } @Step
    public WebElement getLinkNav() throws Throwable{
        try
        {
            linkNav.isCurrentlyVisible();
            List <WebElementFacade> links = linkNav.thenFindAll(By.tagName("li"));
            for (WebElement li : links){
                if (li.getAttribute("innerText").trim().equalsIgnoreCase("ICT3621-18-S2")){
                    li.click();
                    Thread.sleep(10000);
                }
            }
        }catch (NoSuchElementException e){
            System.out.print(e.getMessage());
        }
        return linkNav;
    }

//</editor-fold>

//<editor-fold desc="FLIGHT">
    //************************************FLIGHT APPLICATION**********************************************************************


    @Step
    public void flight()throws Throwable{
        try
        {

            //FROM AIRPORT
            WebElementFacade fromAirport = searchCriteriaWrapper.findBy(By.xpath("//div[@id='fromAirportWrapper']//span[@class='selectBox-arrow icon-angle-down']"));
            highLighterMethod(fromAirport);
            fromAirport.click();
            //SELECT DEPARTURE
            WebElement depart = searchCriteriaWrapper.findElement(By.xpath("//ul[@class='selectBox-dropdown-menu selectBox-options criteriaSelect-selectBox-dropdown-menu selectDestination-selectBox-dropdown-menu selectBox-selectBox-dropdown-menu selectBox-options-bottom']"));
            List <WebElement> selected = depart.findElements(By.xpath("/html[1]/body[1]/ul[1]/li[6]/a[1]"));
            for (WebElement li : selected){
                if (element(li).isDisplayed()){
                    li.click();
                }
            }
            // TO AIRPORT
            WebElementFacade toAirportWrapper = searchCriteriaWrapper.find(By.xpath("//div[@id='toAirportWrapper']"));
            highLighterMethod(toAirportWrapper);
            toAirportWrapper.click();
            //SELECT TO
            WebElement to = searchCriteriaWrapper.findElement(By.xpath("//button[@id='thy-940']"));
            highLighterBorderMethod(to);
            List <WebElement> toSelected = to.findElements(By.xpath("/html[1]/body[1]/ul[14]"));
            for (WebElement li : toSelected){
                if (li.isDisplayed()){
                    li.click();
                }
            }
        }catch (Exception e) {
            e.getMessage();
        }
    }

    @Step
    public  WebElementFacade getElement(){
        linkNav.isCurrentlyVisible();
        return linkNav;
    }
    //GET THE CURRENT DAY
    private String getCurrentDay() {
        //CREATE A CALENDER OBJECT
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());

        //GET CURRENT DAY AS A NUMBER
        int todayInt = calendar.get(Calendar.DAY_OF_MONTH);
        System.out.println("Today int:" + todayInt + "\n");

        //CONVERT INTEGER TO STRING
        String todayStr = Integer.toString(todayInt);
        System.out.println("Today Str:"+todayStr+"\n");

        return todayStr;
    }

    //PICK A DATE FROM THE CALENDER
    @Step
    public void datePicker()throws Throwable{
//        //CLICK THE INITIALIZED WARNING PAGE
//        try
//        {
//            cookieWarningPage();
//            }catch (Exception e){
//            e.printStackTrace();
//        }

        //GET TODAY'S NUMBER
        today = getCurrentDay();
        System.out.println("Today;s number is:"+ today+"\n");

        //Click and open the datepickers
        WebElement calender = myDriver.findElement(By.xpath("//span[@id='calendarPlaceholder']"));
        element(calender).click();

        WebElement calWrapper = myDriver.findElement(By.xpath("//div[@class='wrapper']"));
        scrollIntoElement(calWrapper);

        //This is from date picker table
        WebElement dateWidgetFrom = myDriver.findElement(By.xpath(".//*[@id='calendar-100000000']/div[3]/table/tbody"));

        //This are the rows of the from date picker table
        //List<WebElement> rows = dateWidgetFrom.findElements(By.tagName("tr"));

        //This are the columns of the from date picker table
        List<WebElement> columns = dateWidgetFrom.findElements(By.tagName("td"));

        //DatePicker is a table. Thus we can navigate to each cell
        //and if a cell matches with the current date then we will click it.
        for (WebElement cell: columns) {
            /*
            //If you want to click 18th Date
            if (cell.getText().equals("18")) {
            */
            //Select Today's Date
            if (cell.getText().equals(today)) {
                cell.click();
                break;
            }

        }
    }
    @Step
    public void departureDate()throws Throwable{
        WebElementFacade departureDate = searchCriteriaWrapper.findBy("//input[@id='DepartureDate']");
        highLighterMethod(departureDate);
        //SELECT DEPARTURE DATE
        // departureDate.click();
    }
    @Step
    public void returnDate()throws Throwable{
        WebElementFacade returnDate = searchCriteriaWrapper.findBy("//input[@id='ReturnDate']");
        highLighterMethod(returnDate);
        //SELECT RETURN DATE
        //returnDate.click();
    }
    @Step
    private void cookieWarningPage()throws Throwable {
        WebElement header = myDriver.findElement(By.xpath("/html/body/header"));
        highLighterBorderMethod(header);
        try {
            WebElement container = header.findElement(By.xpath("/html/body/header/div/div"));
            highLighterBorderMethod(container);
            WebElement cookieWarning = container.findElement(By.xpath("//*[@id='errorContent']"));
            highLighterBorderMethod(cookieWarning);
            WebElement close = cookieWarning.findElement(By.xpath("//*[@id='cookieWarningMessage']"));
            WebElement closeX = close.findElement(By.xpath("//*[@id='cookieWarningCloseId']"));
            scrollIntoElement(closeX);
            JavaScriptClickElement(closeX);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


//</editor-fold>


}

